# ML-models-test

This is a space to test ML models:

* PCA
* Random Forest
* SVM
* KNN
* MDS
* Decission trees
* Clustering

When uploading your test, please make sure to add the data set used to run the code :) 