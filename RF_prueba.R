# Cargamos librerías necesarias.

library(dplyr)
library(tidyverse)
library(ggpubr)
library(tidyr)
library(ggplot2)
library(caret)
library(rpart)
library(rpart.plot)
library(rsample)
library(MLmetrics)
library(randomForest)

#Cargamos los datos
path <- '/Users/Victoria/Desktop/master_2020/ML1/Practica/Training_Philipines.csv'
datos <-read.csv(path)

#Pasamos la variable RTvsNT de factor a número (es un ratio).
RTvsNT_c <- as.character(datos$RTvsNT)
RTvsNT_car <- gsub(",", ".", RTvsNT_c)
options(digits=6)
RTvsNT_d <- as.numeric(RTvsNT_car)
datos$RTvsNT <- RTvsNT_d

#Categorizamos la variable T.H.I (nivel socioeconómico alto, medio, bajo).
datos$income_category <- cut(datos$T.H.I, breaks=c(0,350000,700000,1000000000), labels=c("Bajo","Medio","Alto"))
datos = datos %>% select(-T.H.I)

set.seed(123)

#Dividimos en train y test
training <- createDataPartition(datos$income_category, p = .7, list = FALSE, times = 1)
datos_train <- slice(datos, training)
datos_test <- slice(datos, -training)



#1. Ajustes por defecto
#Probamos el ajuste de parámetros con el paquete caret. En primer lugar, vemos el resultado con los ajustes por defecto. 
#Definimos la validación cruzada de 10 iteraciones y con un método de remuestreo de repetidas divisiones de training y test.
rfControl <- trainControl(method = "cv",
                          number = 10,
                          search = "grid")
#Entrenamos el modelo
rf_default <- train(income_category~.,
                    data = datos_train,
                    method = "rf",
                    metric = "Accuracy",
                    trControl = rfControl)
print(rf_default)


#Comenzamos a ajustar nuestro modelo.
#2.Buscamos el número de variables óptimo  elegidas al azar en cada división.
# HIPERPARÁMETROS, NÚMERO DE REPETICIONES Y SEMILLAS PARA CADA REPETICIÓN
#===============================================================================
particiones  <- 10
repeticiones <- 2

# Hiperparámetros
hiperparametros <- expand.grid(mtry = c(5, 7, 10, 12),
                               min.node.size = c(2, 3, 4, 5, 10, 15, 20, 30),
                               splitrule = "gini")

set.seed(123)
seeds <- vector(mode = "list", length = (particiones * repeticiones) + 1)
for (i in 1:(particiones * repeticiones)) {
  seeds[[i]] <- sample.int(1000, nrow(hiperparametros))
}
seeds[[(particiones * repeticiones) + 1]] <- sample.int(1000, 1)

# DEFINICIÓN DEL ENTRENAMIENTO
#===============================================================================
control_train_rf <- trainControl(method = "repeatedcv", number = particiones,
                              repeats = repeticiones, seeds = seeds,
                              returnResamp = "final", verboseIter = FALSE)

# AJUSTE DEL MODELO
# ==============================================================================
set.seed(342)
modelo_rf <- train(income_category ~ BYS + GL + T.F.E + Tra.Exp + Motorvehicle + I.H.R.V + PersonalComputers + CYP + MCareExp + Region + CellularPhone + PPN , data = datos_train,
                   method = "ranger",
                   tuneGrid = hiperparametros,
                   metric = "Accuracy",
                   trControl = control_train_rf,
                   # Número de árboles ajustados
                   num.trees = 200)
modelo_rf
modelo_rf$finalModel
ggplot(modelo_rf, highlight = TRUE) +
  scale_x_continuous(breaks = 1:30) +
  labs(title = "Evolución del accuracy del modelo Random Forest") +
  guides(color = guide_legend(title = "mtry"),
         shape = guide_legend(title = "mtry")) +
  theme_bw()

rfControl <- trainControl(method = "cv",
                          number = 10,
                          search = "grid")
#Entrenamos el modelo con los ajustes óptimos y lo evaluamos con los datos de test.
income_rf_tun = randomForest(income_category~., data=datos_train, ntree=200, mtry=5,nodesize=10 )
income_rf_tun
plot(income_rf_tun)
table_test_rf_tun=table(pred = predict(income_rf_tun, datos_test,type = "class"), obs = datos_test$income_category)
accuracy_test_rf_tun <- sum(diag(table_test_rf_tun)) / sum(table_test_rf_tun)
accuracy_test_rf_tun #Mejora un 1,5 % sobre el árbol de decisión.

#Finalmente, identificamos los predictores más influyentes del modelo

library(tidyverse)
library(ggpubr)
importancia_pred <- as.data.frame(importance(income_rf_tun, scale = TRUE))
importancia_pred <- rownames_to_column(importancia_pred, var = "variable")
p1 <- ggplot(data = importancia_pred, aes(x = reorder(variable, MeanDecreaseGini),
                                          y = MeanDecreaseGini,
                                          fill = MeanDecreaseGini)) +
  labs(x = "Variables", title = "Reducción de pureza (Gini)") +
  geom_col() +
  coord_flip() +
  theme_bw() +
  theme(legend.position = "bottom")
p1

#PROX: añadir matriz de coste. Hacer el random solo con las variables importantes y ver como varía.
#Añadir comentarios


