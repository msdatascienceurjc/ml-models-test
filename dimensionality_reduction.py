# IMPORTS
import numpy as np
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.preprocessing import StandardScaler

# Cargamos los datos
X = pd.read_csv("../data-sets/philippine-income.csv")

# Dividimos nuestro dataset en train y test al 80% y 20%
xTrain, xTest = train_test_split(X, test_size=0.2, random_state=0)

# Extraemos solo las variables numéricas de nuestro xTrain y xTest
xtrainnum = xTrain.select_dtypes('number')
xtestnum = xTest.select_dtypes('number')

# PCA sin estandarizado previo
# Estudio previo - Visualización
pca = PCA(n_components=0.95, svd_solver='full')
xTrain_pca = pca.fit_transform(xtrainnum)
xTest_pca = pca.transform(xtestnum)
plt.plot(np.arange(1, len(pca.explained_variance_ratio_) + 1), np.cumsum(pca.explained_variance_ratio_))
plt.xlabel('n components')
plt.ylabel('cumulative variance')
plt.show()
plt.scatter(xTrain_pca[:, 0], xTrain_pca[:, 1])
plt.show()

# Categorizamos la variables target
data = X


def inc_groups(series):
    if series < 35000:
        return "Bajo"
    elif 35000 <= series < 700000:
        return "Medio"
    elif 700000 <= series:
        return "Alto"

data['T.H.I'] = data['T.H.I'].apply(inc_groups)

data['T.H.I'].value_counts(sort=False)

y = data[['T.H.I']]
X = data.drop('T.H.I', axis=1)

# Dividimos nuestro dataset en train y test al 70% y 30%
xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=0.30, random_state=1, stratify=y)

# Solo las que són numéricas
xtrainnum = xTrain.select_dtypes('number')
xtestnum = xTest.select_dtypes('number')
sc = StandardScaler()
# Gerardo - Standard Scaler u=0 sigma=1
xtrainnum = sc.fit_transform(xtrainnum)
xtestnum = sc.transform(xtestnum)
# PCA sobre las variables independientes
pca = PCA(n_components=2, svd_solver='full')
sample_train_pca_ger = pca.fit_transform(xtrainnum)
sample_test_pca_ger = pca.transform(xtestnum)
print('Porcentaje de la varianza explicada: \t', pca.explained_variance_ratio_)
print('Coordenadas de las componentes principales: \n', pca.components_)
print('Autovalores asociados a las componentes: \t', pca.singular_values_)

pca_components = pd.DataFrame(data=sample_test_pca_ger, columns=['PC1', 'PC2'])
ax = sns.scatterplot(x="PC1", y="PC2", hue=yTest['T.H.I'],
                     data=pca_components)
plt.show()

# --------------------------------------------------------------------------------
# Cargamos los datos
X = pd.read_csv("../data-sets/philippine-income.csv")
y = X[['Region']]
# Dividimos nuestro dataset en train y test al 80% y 20%
xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=0.30, random_state=0, stratify=y)

# Extraemos solo las variables numéricas de nuestro xTrain y xTest
xtrainnum = xTrain.select_dtypes('number')
xtestnum = xTest.select_dtypes('number')

from sklearn.manifold import Isomap

dr = PCA(n_components=0.90, svd_solver='full')
xtrainnum_iso = dr.fit_transform(xtrainnum)
xtestnum_iso = dr.transform(xtestnum)
# iso_components = pd.DataFrame(data=xtrainnum_iso, columns=['PC1', 'PC2'])
# ax = sns.scatterplot(x="PC1", y="PC2", hue=yTrain['Region'],
#                      data=iso_components)
plt.show()
from sklearn.neighbors import KNeighborsClassifier

neig_k = np.arange(1, 23, 2)
scores = []
for k in neig_k:
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(xtrainnum_iso, yTrain.values.ravel())
    predictions_KNN = knn.predict(xtestnum_iso)
    scores.append(metrics.accuracy_score(yTest.values.ravel(), predictions_KNN))
print('El valor máximo de accuracy se consigue para un k de : ', neig_k[scores.index(max(scores))],
      'con un valor de', max(scores))
print('Porcentaje de la varianza explicada: \t', dr.explained_variance_ratio_)
print('Coordenadas de las componentes principales: \n', dr.components_)
print('Autovalores asociados a las componentes: \t', dr.singular_values_)
