# Carga de paquetes y librerias a utilizar
import os
import time
import sys

import pandas as pd
from pandas.api.types import is_numeric_dtype
import numpy as np
from matplotlib import pyplot as plt

import sklearn
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import load_files
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn import linear_model
from sklearn import metrics
from sklearn.naive_bayes import MultinomialNB, ComplementNB, BernoulliNB, GaussianNB
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn import preprocessing
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.decomposition import PCA
from sklearn.manifold import MDS, TSNE
from sklearn.cluster import KMeans, DBSCAN, AgglomerativeClustering
import scipy.cluster.hierarchy as shc

# Variables globales
texto_error = "\n\nEntrada incorrecta. Por favor, vuelve a intentarlo.\n\n"
porcentaje = 0.10


def compararClasificadores(clas_opcion):

    if (clas_opcion == "0"):
        nclusters = elegirClusters()
        evaluarClustering(nclusters)

    elif (clas_opcion == "1"):
        nombres = ["Procesamiento Gaussiano"]

        clasificadores = [
            GaussianProcessClassifier(1.0 * RBF(1.0))]

    elif (clas_opcion == "2"):
        nombres = ["AdaBoost"]

        clasificadores = [
            AdaBoostClassifier()]

    elif (clas_opcion == "3"):
        nombres = ["Regresion lineal"]

        clasificadores = [
            linear_model.LinearRegression()]

    elif (clas_opcion == "4"):
        nombres = ["Naive Bayes - GaussianNB", "Naive Bayes - MultinomialNB", "Naive Bayes - ComplementNB",
                   "Naive Bayes - BernoulliNB"]

        clasificadores = [
            GaussianNB(),
            MultinomialNB(),
            ComplementNB(),
            BernoulliNB()]

    elif (clas_opcion == "5"):
        nombres = ["Regresion logistica"]

        clasificadores = [
            linear_model.LogisticRegression(max_iter = 1000)]

    elif(clas_opcion == "6"):
        nombre = "kNN"

        clasificadores = [
            KNeighborsClassifier(1),
            KNeighborsClassifier(2),
            KNeighborsClassifier(3),
            KNeighborsClassifier(4),
            KNeighborsClassifier(5),
            KNeighborsClassifier(6),
            KNeighborsClassifier(7),
            KNeighborsClassifier(8),
            KNeighborsClassifier(9),
            KNeighborsClassifier(10),
            KNeighborsClassifier(11)]


    elif (clas_opcion == "7"):
        nombre = "Arbol de decision"

        clasificadores = [
            DecisionTreeClassifier(max_depth=3),
            DecisionTreeClassifier(max_depth=4),
            DecisionTreeClassifier(max_depth=5),
            DecisionTreeClassifier(max_depth=6),
            DecisionTreeClassifier(max_depth=7),
            DecisionTreeClassifier(max_depth=8),
            DecisionTreeClassifier(max_depth=9),
            DecisionTreeClassifier(max_depth=10)]

    elif(clas_opcion == "8"):
        nombres = ["max_depth=5 y n_estimators=10",
                   "max_depth=5 y n_estimators=100",
                   "max_depth=10 y n_estimators=10",
                   "max_depth=10 y n_estimators=100",
                   "max_depth=15 y n_estimators=10",
                   "max_depth=15 y n_estimators=100"]

        clasificadores = [
            RandomForestClassifier(max_depth=5, n_estimators=10, criterion='gini'),
            RandomForestClassifier(max_depth=5, n_estimators=100),
            RandomForestClassifier(max_depth=10, n_estimators=10),
            RandomForestClassifier(max_depth=10, n_estimators=100),
            RandomForestClassifier(max_depth=15, n_estimators=10),
            RandomForestClassifier(max_depth=15, n_estimators=100)]

    elif(clas_opcion == "9"):
        nombres = ["alpha=0.0001",
                  "alpha=0.001",
                  "alpha=0.01",
                  "alpha=0.1",
                  "alpha=1"]

        clasificadores = [
            MLPClassifier(alpha=0.0001, max_iter=10000),
            MLPClassifier(alpha=0.001, max_iter=10000),
            MLPClassifier(alpha=0.01, max_iter=10000),
            MLPClassifier(alpha=0.1, max_iter=10000),
            MLPClassifier(alpha=1, max_iter=10000)]

    elif (clas_opcion == "10"):
        nombres = ["SVC kernel lineal y c=0.025", "SVC gamma=2 y C=1"]

        clasificadores = [
            SVC(kernel="linear", C=0.025),
            SVC(gamma=2, C=1)]

    if(clas_opcion != "0"):
        print("\n\nPor favor, espera mientras se evaluan los diferentes modelos...\n\n")

    if(clas_opcion in ["6","7"]):
        for clasificador in clasificadores:
            try:
                clasificador.fit(X_train, y_train.values.ravel())
                y_pred = clasificador.predict(X_test)
                score = metrics.accuracy_score(y_test, y_pred)
                if (score < 0.1):
                    score_num = calcularAccuracyNum(y_test, y_pred)
                    print(nombre + ": accuracy (num) - ", score_num)
                else:
                    print(nombre + ": accuracy - ", score)
            except:
                print(nombre+": no es posible evaluarlo con las opciones elegidas")

    elif (clas_opcion != "0"):
        for nombre, clasificador in zip(nombres, clasificadores):
            try:
                clasificador.fit(X_train, y_train.values.ravel())
                y_pred = clasificador.predict(X_test)
                score = metrics.accuracy_score(y_test, y_pred)

                if (score < 0.1):
                    score_num = calcularAccuracyNum(y_test, y_pred)
                    print(nombre + ": accuracy (num) - ", score_num)
                else:
                    print(nombre + ": accuracy - ", score)
            except:
                print(nombre+": no es posible evaluarlo con las opciones elegidas")

    time.sleep(2)
    print("\n")

def evaluarClustering(nclusters):

    #cluster1 = AgglomerativeClustering(n_clusters=nclusters, affinity='euclidean', linkage='ward')
    #cluster.fit_predict(X_train)

    # K medias
    print("\nComienza el analisis de K medias...\n")
    time.sleep(1)
    medirInercia()

    kmeans = KMeans(n_clusters=nclusters, random_state=0)
    labels_km = kmeans.fit_predict(X_train)

    print("Cluster sizes k-means: {}".format(np.bincount(labels_km)))

    distances = []
    for c in kmeans.cluster_centers_:
        d = np.sum(np.sum((X_train - c) ** 2, axis=1))
        distances.append(d.round(2))

    print("Cluster distances k-means: {}".format(distances))

    plt.figure(figsize=(12, 4))
    plt.title("K-medias")
    plt.subplot(121)
    plt.bar(range(nclusters), np.bincount(labels_km))
    plt.xlabel('Pollas')
    plt.ylabel('Vinagre')

    plt.subplot(122)
    plt.bar(range(nclusters), distances)
    plt.xlabel('caaca')
    plt.ylabel('culo')
    plt.show()

    #medirDistanciaCentroides(X_train, kmeans.cluster_centers_[nclusters-1])

    # DBSCAN
    time.sleep(1)
    print("\nComienza el analisis de DBSCAN...\n")
    time.sleep(1)
    for eps in [1, 3, 5, 7]:
        print("\neps={}".format(eps))
        dbscan = DBSCAN(eps=eps, min_samples=10)
        labels = dbscan.fit_predict(X_train)
        print("Number of clusters: {}".format(len(np.unique(labels))))
        print("Cluster sizes: {}".format(np.bincount(labels + 1)))

    # Clustering jerarquico
    time.sleep(1)
    print("\nComienza el analisis de Clustering Jerarquico...\n")
    time.sleep(1)

    plt.figure(figsize=(10, 7))
    plt.title("Customer Dendograms")
    dend = shc.dendrogram(shc.linkage(X_train.values[:,1:2], method='ward'))
    plt.show()

    cluster = AgglomerativeClustering(n_clusters=nclusters, affinity='euclidean', linkage='ward')
    cluster.fit_predict(X_train)
    plt.figure()
    plt.title("Clustering jerarquico")
    plt.scatter(X_train.values[:,1], X_train.values[:,2], c=cluster.labels_, cmap='rainbow')
    plt.show()

def medirInercia():
    K = range(1, 20)

    inertia = []
    for k in K:
        kmeans = KMeans(n_clusters=k).fit(X_train)
        inertia.append(kmeans.inertia_)

    plt.plot(K, inertia, '.-')
    plt.xlabel('# of clusters')
    plt.ylabel('Inertia')
    plt.show()

def medirDistanciaCentroides(X, centroide, n=5):
    distance = np.sum((X - centroide) ** 2, axis=1)

    print('Close to center')
    display(marketing_data_encoded.iloc[np.argsort(distance)[:n]])

    print('Far from center')
    display(marketing_data_encoded.iloc[np.argsort(distance)[-n:]])

def calcularAccuracyNum(y_test, y_pred):
    acierto = 0
    fallo = 0
    total = 0
    nombre_target = list(y_test)
    y_test_lista = y_test[nombre_target[0]].tolist()

    for y, yy in zip(y_test_lista,y_pred):
        margen = y*porcentaje
        diferencia = abs(y-yy)
        if(diferencia<margen):
            acierto = acierto + 1
        else:
            fallo = fallo + 1
        total = total + 1

    score = acierto/total

    return score

def menuTarget(nombres_dataset):
    entradaInvalida = True
    separador = " -- "
    lista_string = separador.join(nombres_dataset)

    while (entradaInvalida):
        variable = input("\n>>>> Escribe el nombre de la variable target (o escriba 'salir')\n"
                         "- Valores posibles:\n"+lista_string+"\n"
                         "---------------------->>>> ")

        if variable in nombres_dataset:
            entradaInvalida = False
        elif(variable == "salir"):
            entradaInvalida = False
        else:
            print(texto_error)
            time.sleep(2)

    return variable

def menuVariables():
    entradaInvalida = True
    opcionesValidas = ["1", "2", "3", "4","5","6","7"]

    while (entradaInvalida):
        opcion = input("\n>>>> Elige el conjunto de variables que deseas utilizar:\n"
                       " (1) Todas las variables\n"
                       " (2) Todas las variables normalizadas\n"
                       " (3) Sólo las categóricas\n"
                       " (4) Sólo las numéricas\n"
                       " (5) Sólo las numéricas normalizadas\n"
                       " (6) Volver a menu anterior\n"
                       " (7) Salir\n\n"
                       "Escribe una opcion del 1 al 7 --> ")
        if opcion in opcionesValidas:
            entradaInvalida = False
        else:
            print(texto_error)
            time.sleep(2)

    return opcion

def menuClasificadores(opcionVariables):

    entradaInvalida = True
    opcionesValidas = ["0","1","2","3","4","5","6","7","8","9","10"]

    while (entradaInvalida):
        clas_opcion = input("\n>>>> Elige el conjunto de modelos a evaluar:\n"
                       " (0) Clustering (aprendizaje no supervisado)"
                       " (1) Procesamiento Gaussiano\n"
                       " (2) AdaBoost\n"
                       " (3) Regresion lineal\n"
                       " (4) Naive Bayes\n"     
                       " (5) Regresión logistica\n"
                       " (6) kNN con valores de k entre 1 y 11\n"
                       " (7) Arboles de decisión de profundidad entre 3 y 10\n"
                       " (8) Random forests\n"
                       " (9) Redes neuronales\n"
                       " (10) Support Vector Classification\n"
                       " (11) Volver al menu de eleccion de variables\n"
                       " (12) Salir\n\n"
                       "Escribe una opcion del 0 al 12--> ")

        if clas_opcion in opcionesValidas:
            compararClasificadores(clas_opcion)
        elif(clas_opcion in ["11","12"]):
            entradaInvalida = False
        else:
            print(texto_error)
            time.sleep(2)

    return clas_opcion

def custom_dataset(X,opcionVariables):

    X_categ = X.select_dtypes('object')
    X_num = X.select_dtypes('number')
    X_categ_dummies = prepare_categ(X_categ)

    if(opcionVariables == "3"):
        X_custom = X_categ
        X_dummies = X_categ_dummies
    elif(opcionVariables in ["4","5"]):
        X_custom = X_num
        X_dummies = X_num
    else:
        X_custom = X_num.join(X_categ)
        X_dummies = X_num.join(X_categ_dummies)

    return X_custom, X_dummies

def prepare_categ (X):
    for name in list(X):
        X = pd.get_dummies(X, columns = [name], drop_first = True)

    return X

def elegirClusters ():
    noRespuesta = True
    while (noRespuesta):
        try:
            nclusters = int(input("\n >>>> Introduce el numero de clusters que quieres (entre 2 y 14): "))
            min = 1
            max = 15
            if min > nclusters or nclusters > max:
                print(texto_error)
            else:
                noRespuesta = False

        except ValueError:
            print(texto_error)

    return nclusters

def elegirReduccion (X_train,X_test):
    noRespuesta = True
    #print(X_train)
    #print(X_test)
    while (noRespuesta):
        try:
            metodo = input("\n\n >>>> Selecciona el método de reduccion de dimensionalidad que quieres aplicar:\n"
                           " (1) PCA\n"
                           " (2) MDS\n"
                           " (3) t-SNE\n"
                           " (4) No usar ninguno\n"
                           " --> Escribe aqui tu opcion elegida: ")

            noRespuesta = False

            if(metodo in ["1","2","3"]):
                min = 0
                if(metodo == "3"):
                    ncomp = int(input("\n >>>> Introduzca el numero de variables entre 1 y 3: "))
                    max = 4
                else:
                    ncomp = int(input("\n >>>> Introduzca el numero de variables entre 1 y 9: "))
                    max = 10

                if min < ncomp and ncomp < max:
                    if(metodo == "1"):
                        metodoAplicado = PCA(n_components=ncomp)
                        metodo = "PCA"
                    elif (metodo == "2"):
                        metodoAplicado = MDS(n_components=ncomp)
                        metodo = "MDS"
                    else:
                        metodoAplicado = TSNE(n_components=ncomp)
                        metodo = "t-SNE"

                    print("\nHas elegido aplicar " + metodo + " con " + str(ncomp) + " variables\n")
                    X_train = metodoAplicado.fit_transform(X_train)
                    X_test = metodoAplicado.fit_transform(X_test)
                    time.sleep(2)

                else:
                    print(texto_error)
                    noRespuesta = True

            elif(metodo == "4"):
                print("\nHas elegido no aplicar ningun metodo de reduccion de la dimensionalidad\n")
                time.sleep(2)

            else:
                print(texto_error)
                noRespuesta = True

        except ValueError:
            print(texto_error)
            noRespuesta = True

        except:
            print("Error inesperado:", sys.exc_info())
            noRespuesta = True

    #print(X_train)
    #print(X_test)
    return X_train, X_test

if __name__ == "__main__":

    print("\n\n--------- Bienvenid@ al sistema evaluador de modelos de Machine Learning --------- \n")

    online = True

    while(online):
        # Extraccion del dataset del csv
        dataset = pd.read_csv("philippine-income.csv")
        dataset = dataset.sample(n=40)

        # Menu de usuario para elegir la variable target
        target_name = menuTarget(list(dataset))

        online2 = True
        if (target_name != "salir"):
            if is_numeric_dtype(dataset[target_name]):
                print("\nHas elegido una variable numérica. El resultado del accuracy se medirá\n"
                      "tomando como acierto aquel valor predicho que difiera\n"
                      "del real en menos de un " + str(porcentaje * 100) + "%")
                time.sleep(5)

            while(online2):
                # Guardamos el conjunto de clustering
                X_clustering_no, X_clustering = custom_dataset(dataset,"1")

                # Definimos nuestra variable target
                y = dataset[[target_name]]
                X = dataset.drop(target_name, axis=1)

                opcionVariables = menuVariables()

                if(opcionVariables not in ["6","7"]):
                    # Preparacion del dataset personalizado
                    X_custom, X_dumm = custom_dataset(X,opcionVariables)

                    # División del conjunto de datos en 20% test y 80% entrenamiento
                    X_train, X_test, y_train, y_test = train_test_split(X_dumm, y, test_size=0.2, random_state=0)

                    # Normalizamos nuestros datos con la función
                    if(opcionVariables in ["2","5"]):
                        X_train = preprocessing.scale(X_train)
                        X_test = preprocessing.scale(X_test)

                    # Llamamos a metodo para elegir si aplicar algun método de reducción de dimensionalidad
                    X_train, X_test = elegirReduccion(X_train, X_test)

                    # Construccion y evaluacion de diferentes modelos de clasificacion
                    clas_opcion = menuClasificadores(opcionVariables)

                    if(clas_opcion == "11"):
                        online2 = True
                    else:
                        online = False
                        online2 = False
                elif(opcionVariables == "6"):
                    online2 = False
                else:
                    online = False
                    online2 = False
        else:
            online = False

    print("\n\n >>>>>>>>>>>> ¡Hasta luego, datascientist! <<<<<<<<<<<< ")
    print("\n\n >>>>>>>>>>>> ¡Vuelve pronto! <<<<<<<<<<<< ")