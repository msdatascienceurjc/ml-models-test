import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import StratifiedShuffleSplit
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
# Gerardo
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import numpy as np
import seaborn as sns

from sklearn.neighbors import KNeighborsClassifier

from sklearn import metrics
from sklearn import preprocessing

# lógica

# carga datos de train
data_train = pd.read_csv("../data-sets/datos_train.csv")
yTrain = data_train[['T.H.I']]
xTrain = data_train.drop('T.H.I', axis=1)

# carga datos de test
data_test = pd.read_csv("../data-sets/datos_test.csv")
yTest = data_test[['T.H.I']]
xTest = data_test.drop('T.H.I', axis=1)

# print('Se muestran cuántas muestras hay de cada cada categoría:\n', data.groupby('T.H.I').size())

# Consejo de Victor --> Cross Validation o validación

# Miramos si hemos dividido bien las clases
print('Distribución de las clases en las muestras de entrenamiento:\n', yTrain.groupby('T.H.I').size())
print('Distribución de las clases en las muestras de test:\n', yTest.groupby('T.H.I').size())

# Extraemos solo las variables numéricas de nuestro xTrain y xTest
xtrainnum_df = xTrain.select_dtypes('number')
xtestnum_df = xTest.select_dtypes('number')
# -------------------------------- Estandarización -------------------------------------
sc = StandardScaler()
# Gerardo - Standard Scaler u=0 sigma=1
xtrainnum = sc.fit_transform(xtrainnum_df)
xtestnum = sc.transform(xtestnum_df)
# -------------------------------- PCA -------------------------------------

pca = PCA(n_components=1, svd_solver='full')
xtrainnum_pca = pca.fit_transform(xtrainnum)
xtestnum_pca = pca.transform(xtestnum)

print('Porcentaje de la varianza explicada: \t', pca.explained_variance_ratio_)
# print(pca.explained_variance_)
componentes = pca.components_
print('Coordenadas de las componentes principales: \n', componentes)
print('Autovalores asociados a las componentes: \t', pca.singular_values_)

# -------------------------------- Prueba sin estudio de las variables -------------------------------------
neig_k = np.arange(1, 23, 2)
scores = []
for k in neig_k:
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(xtrainnum_pca, yTrain.values.ravel())
    predictions_KNN = knn.predict(xtestnum_pca)
    scores.append(metrics.accuracy_score(yTest, predictions_KNN))
print('El valor máximo de accuracy se consigue para un k de : ', neig_k[scores.index(max(scores))],
      'con un valor de', max(scores))

print('Valores de accuracy:\n', scores)
knn = KNeighborsClassifier(n_neighbors=neig_k[scores.index(max(scores))])
knn.fit(xtrainnum_pca, yTrain.values.ravel())
predictions_KNN = knn.predict(xtestnum_pca)
# Metricas
print(metrics.accuracy_score(yTest, predictions_KNN))
print(metrics.confusion_matrix(yTest, predictions_KNN))
print(metrics.classification_report(yTest, predictions_KNN))
# -------------------------------- Variables de interés -------------------------------------
# Seleccionamos las variables cuyo threshold es mayor a 0.3
th = 0.3
index = np.where(componentes[0, :] > th)[0]
variables = list(xtrainnum_df.columns[index.tolist()])
print('Las variables que se eligen para la prueba del modelo son: ', variables)
# lambda x : x > th
# hola = df_componentes.apply(lambda x: x (if x > 0.3), axis=0)
# -------------------------------- Prueba con estudio de las variables ----------------------
xtrainnum_df = xtrainnum_df[variables]
xtestnum_df = xtestnum_df[variables]

xtrainnum = sc.fit_transform(xtrainnum_df)
xtestnum = sc.transform(xtestnum_df)
# --- knn---
neig_k = np.arange(1, 23, 2)
scores = []
for k in neig_k:
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(xtrainnum, yTrain.values.ravel())
    predictions_KNN = knn.predict(xtestnum)
    scores.append(metrics.accuracy_score(yTest, predictions_KNN))
print('El valor máximo de accuracy se consigue para un k de : ', neig_k[scores.index(max(scores))],
      'con un valor de', max(scores))

print('Valores de accuracy:\n', scores)
#neig_k[scores.index(max(scores))]
knn = KNeighborsClassifier(n_neighbors=3)
knn.fit(xtrainnum, yTrain.values.ravel())
predictions_KNN = knn.predict(xtestnum)
# Metricas
print(metrics.accuracy_score(yTest, predictions_KNN))
print(metrics.confusion_matrix(yTest, predictions_KNN))
print(metrics.classification_report(yTest, predictions_KNN))


