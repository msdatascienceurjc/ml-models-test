import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn import preprocessing

data = pd.read_csv("datos-new.csv")

#Definimos nuestra variable target

y = data[['Tobacco']]
X = data.drop('Tobacco', axis = 1)
print(y.head())
print(X.head())

#Dividimos nuestro dataset en train y test al 80% y 20%
xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Extraemos solo las variables numéricas de nuestro xTrain y xTest

xtrainnum = xTrain.select_dtypes('number')
xtestnum = xTest.select_dtypes('number')

# Normalizamos nuestros datos con la función

normalized_xtrain = preprocessing.scale(xtrainnum)
normalizaed_xtest = preprocessing.scale(xtestnum)

#Importamos el modelo KNN
from sklearn.neighbors import KNeighborsClassifier

#Creamos el clasificador con K = 2
knn = KNeighborsClassifier(n_neighbors=2)

#Entrenamos el modelo con la funcion fit
knn.fit(normalized_xtrain, yTrain)

#Hacemos las predicciones
y_pred = knn.predict(normalizaed_xtest)

#Importamos el modulo de scikit-learn metrics module para calcular su accuracy
from sklearn import metrics

# Model Accuracy
print("Accuracy:",metrics.accuracy_score(yTest, y_pred))



