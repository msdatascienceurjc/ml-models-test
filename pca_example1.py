# PCA for fun
# We will be using the Wine dataset from The UCI Machine Learning Repository
# in our example. This dataset consists of 178 wine samples with 13 features
# describing their different chemical properties.

# The first feature is the type of wine (I Think) and the remaining 12 are chemical
# features. Then We Have 178 rows and 12 chemical features clasified in 3 groups

# For load
import pandas as pd
# For split
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
# For dimensionality reduction
from sklearn.decomposition import PCA
#For multinomial logistic regression
from sklearn.linear_model import LogisticRegression
# For metrics
from sklearn.metrics import confusion_matrix, accuracy_score
# Load data
df_wine = pd.read_csv('../data-sets/wine.data', header=None)
print(df_wine.head())

# Split data into train an test --> 70% Train 30% Test
Samples, labels = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values

samples_train, samples_test, labels_train, labels_test = train_test_split(
    Samples, labels, test_size=0.3,
    stratify=labels, random_state=0)

# standardize the features
sc = StandardScaler()
samples_train_std = sc.fit_transform(samples_train)
samples_test_std = sc.transform(samples_test)

pca = PCA(n_components=2)
mlr = LogisticRegression(multi_class='auto')

Sample_train_pca = pca.fit_transform(samples_train_std)
Sample_test_pca = pca.transform(samples_test_std)

# Train multinomial logistic regression
mlr.fit(Sample_train_pca, labels_train)
labels_predict = mlr.predict(Sample_test_pca)
# Metrics
print(confusion_matrix(labels_test, labels_predict))
print("accuracy", accuracy_score(labels_test, labels_predict))

# Explained variance ratio of the principal components
pca = PCA(n_components=None)
X_train_pca = pca.fit_transform(samples_train_std)
print(pca.explained_variance_ratio_)
print(pca.explained_variance_)

# Other techniques for dimensionality reduction are
# Linear Discriminant Analysis (LDA)
# and Kernel PCA (used for non-linearly separable data).