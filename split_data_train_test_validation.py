from sklearn.model_selection import train_test_split
import pandas as pd

data = pd.read_csv("../data-sets/total_datos_philipines.csv")

# Dijo victor que había que justificarlos --> hacer alguna alguna gráfica

# --------------------------------------------------------------------------------------------------------
# Para el clustering es necesario generar los csv con los valores originales
# Duda: Nuestra división de los datos debería ir en el siguiente orden:
# --> División en Train y test sin hacer la división entre secciones del income para estudiar el income
# --> Posteriormente hacer la categorización del income tanto en train como en test y también en validación

# Se propone generar los mismos csv's pero con valores numéricos
# xTrain, xVal = train_test_split(data, test_size=0.20, random_state=1)
#
# xTrain, xTest = train_test_split(xTrain, test_size=0.30, random_state=1)
#
# xTrain.to_csv(path_or_buf="../data-sets/datos_entrenamiento_numerico.csv", index=False)
# xTest.to_csv(path_or_buf="../data-sets/datos_test_numerico.csv", index=False)
# xVal.to_csv(path_or_buf="../data-sets/datos_validacion_numerico.csv", index=False)
# --------------------------------------------------------------------------------------------------------

# Para la categorización de la variable income
def inc_groups(series):
    if series < 350000:
        return "Bajo"
    elif 350000 <= series < 700000:
        return "Medio"
    elif 700000 <= series:
        return "Alto"


data['T.H.I'] = data['T.H.I'].apply(inc_groups)

y = data[['T.H.I']]
X = data.drop('T.H.I', axis=1)

xTrain, xVal, yTrain, yVal = train_test_split(X, y, test_size=0.20, random_state=1, stratify=y)

xTrain, xTest, yTrain, yTest = train_test_split(xTrain, yTrain, test_size=0.30, random_state=1, stratify=yTrain)



datos_validacion = pd.concat([yVal, xVal], axis=1)
datos_train = pd.concat([yTrain, xTrain], axis=1)
datos_test = pd.concat([yTest, xTest], axis=1)

datos_validacion.to_csv(path_or_buf="../data-sets/datos_validacion.csv", index=False)
datos_train.to_csv(path_or_buf="../data-sets/datos_train.csv", index=False)
datos_test.to_csv(path_or_buf="../data-sets/datos_test.csv", index=False)

# Mostrar las proporciones de cada división
print('Proporciones en los datos de validación:\n', datos_validacion['T.H.I'].value_counts(sort=False), end="\n")
print('Proporciones en los datos de train:\n', datos_train['T.H.I'].value_counts(sort=False), end="\n")
print('Proporciones en los datos de test:\n', datos_test['T.H.I'].value_counts(sort=False), end="\n")
